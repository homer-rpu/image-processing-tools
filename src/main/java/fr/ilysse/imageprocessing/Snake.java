package fr.ilysse.imageprocessing;

import com.google.common.base.Stopwatch;
import com.sun.imageio.plugins.common.ImageUtil;
import fr.ilysse.imageprocessing.image.ImageUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;

/**
 * Created by pierre on 15/05/17.
 */
public class Snake {

    public static final Logger LOGGER = LoggerFactory.getLogger(Snake.class);
    public static final String FILE_NAME = "snake";

    public static void main(String[] args) {
        Stopwatch timer = Stopwatch.createStarted();
        if (args.length < 1) {
            LOGGER.error("Missing application properties file");
            System.exit(-1);
        }

        Properties properties = null;
        try {
            properties = new Properties();
            properties.load(java.nio.file.Files.newInputStream(Paths.get(args[0])));
        } catch (IOException e) {
            LOGGER.error("Unable to read application properties file", e);
            System.exit(-1);
        }

        String sourceFolderString = properties.getProperty("targetFolder");

        final List<BufferedImage> snakeImagesFromInitialCircle = ImageUtils.getSnakeImagesFromInitialCircle(500, 500, 200, 10);

        try {
            int i = 0;
            for (BufferedImage image : snakeImagesFromInitialCircle) {
                String snakeImage = sourceFolderString + "/" + i + "_" + FILE_NAME + ".jpg";
                ImageIO.write(image, "jpg", Paths.get(snakeImage).toFile());
                i++;
            }
        } catch (IOException e) {
            LOGGER.error("Error while writing image {}", e);
        }
    }


}
