package fr.ilysse.imageprocessing;

import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.io.Files;
import fr.ilysse.imageprocessing.data.FileData;
import fr.ilysse.imageprocessing.data.Template;
import fr.ilysse.imageprocessing.image.ImageUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by p_poucif on 05/04/2017.
 */
public class BeauteApp {

    public static final Logger LOGGER = LoggerFactory.getLogger(BeauteApp.class);
    public static final Collection<String> EXTENSIONS = Arrays.asList("jpg", "png", "jpeg");
    public static final String GUN_SEPARATOR = "_";
    public static final String GUN_PREFIX = "G_";
    public static final String ZP = "_ZP_";

    public static void main(String[] args) {

        Stopwatch timer = Stopwatch.createStarted();
        if (args.length < 1) {
            LOGGER.error("Missing application properties file");
            System.exit(-1);
        }

        Properties properties = null;
        try {
            properties = new Properties();
            properties.load(java.nio.file.Files.newInputStream(Paths.get(args[0])));
        } catch (IOException e) {
            LOGGER.error("Unable to read application properties file", e);
            System.exit(-1);
        }

        String sourceFolderString = properties.getProperty("sourceFolder");
        String targetFolderOK = properties.getProperty("targetFolderOK");
        String targetFolderKO = properties.getProperty("targetFolderKO");
//        final Map<String, Integer> xyDatas = Maps.newHashMap();
//        String xMin = properties.getProperty("xmin");
//        xyDatas.put("xMin", Integer.parseInt(xMin));
//        String xMax = properties.getProperty("xmax");
//        xyDatas.put("xMax", Integer.parseInt(xMax));
//        String yMin = properties.getProperty("ymin");
//        xyDatas.put("yMin", Integer.parseInt(yMin));
//        String yMax = properties.getProperty("ymax");
//        xyDatas.put("yMax", Integer.parseInt(yMax));

        Path sourcePath = Paths.get(sourceFolderString);
        File sourceFolder = sourcePath.toFile();
        if (sourceFolder.exists()) {

            int imageProcessedCount = 0;
            int gunImage = 0;
            int supplierImage = 0;
            int errorImage = 0;

            final File[] files = sourceFolder.listFiles();

            Map<String, List<FileData>> imagesMappedToUgProduct = Maps.newHashMap();

            Stopwatch fileTimer = Stopwatch.createStarted();

            for (final File file : files) {
                if (EXTENSIONS.contains(Files.getFileExtension(file.getName()))) {

                    String ugProduct = null;
                    FileData fileData = null;

                    if (StringUtils.startsWith(file.getName(), GUN_PREFIX)) {
                        final String[] split = Files.getNameWithoutExtension(file.getName()).split(GUN_SEPARATOR);
                        if (split.length == 5) {
                            ugProduct = split[1] + GUN_SEPARATOR + split[4];
                            final String resultImageName = GUN_PREFIX + split[1] + GUN_SEPARATOR + split[2] + ZP + split[4];
                            fileData = new FileData(file, resultImageName, Files.getFileExtension(file.getName()));
                            gunImage++;
                        } else {
                            LOGGER.warn("Incorrect gun file name {}", file.getName());
                            errorImage++;
                        }
                    } else {
                        final String[] split = file.getName().split(GUN_SEPARATOR);
                        if (split.length == 3) {
                            ugProduct = split[0] + GUN_SEPARATOR + split[2];

                            final String resultImageName = GUN_PREFIX + split[0] + GUN_SEPARATOR + split[1] + GUN_SEPARATOR + split[2];
                            fileData = new FileData(file, resultImageName, Files.getFileExtension(file.getName()));

                            supplierImage++;
                        } else {
                            LOGGER.warn("Incorrect supplier file name {}", file.getName());
                            errorImage++;
                        }


                    }

                    if (StringUtils.isNoneBlank(ugProduct) && fileData != null) {
                        if (imagesMappedToUgProduct.containsKey(ugProduct)) {
                            imagesMappedToUgProduct.get(ugProduct).add(fileData);
                        } else {
                            imagesMappedToUgProduct.put(ugProduct, Lists.newArrayList(fileData));
                        }
                    }

                    imageProcessedCount++;
                }
            }

            LOGGER.info("{} image(s) found over {} file(s) in {} in {}", imageProcessedCount, files.length, sourceFolderString, fileTimer.stop());
            LOGGER.warn("{} error image(s) found", errorImage);
            LOGGER.info("*****************************************************");
            LOGGER.info("************* Starting Image Processing *************");
            LOGGER.info("*****************************************************");

            imagesMappedToUgProduct.entrySet().stream()
                    .forEach(stringListEntry -> {
                        LOGGER.info("//////////////// Starting to process {} image(s) for {} ////////////////", stringListEntry.getValue().size(), stringListEntry.getKey());
                        final Stopwatch productTimer = Stopwatch.createStarted();

                        final List<FileData> fileDatas = stringListEntry.getValue();
//                        final List<BufferedImage> bufferedImageList = Lists.newArrayList();
                        fileDatas.stream()
                                .forEach(fileData -> {
                                    try {
                                        final Stopwatch imageTimer = Stopwatch.createStarted();
//                                        final BufferedImage bufferedImage = JPEGCodec.createJPEGDecoder(new FileInputStream(fileData.getFile())).decodeAsBufferedImage();
                                        final BufferedImage bufferedImage = ImageIO.read(fileData.getFile());
                                        final BufferedImage computedImageFromBounds = ImageUtils.getComputedImage(bufferedImage,
                                                Template.F9.getWidth(), Template.F9.getHeight(), 800, 1100, 1200);

//                                        final BufferedImage computedImageFromBounds = ImageUtils.getComputedImageFromBounds(bufferedImage,
//                                                Template.F9.getWidth(), Template.F9.getHeight(), Template.F9.getMargin(), 1100, 1200, xyDatas);
//                                        fileData.setBufferedImage(bufferedImage);
//                                        bufferedImageList.add(bufferedImage);

                                        LOGGER.info("{} processed in {}", fileData.getResultImageName(), imageTimer.stop());
                                        final File resultFile = new File(targetFolderOK + "/" + fileData.getResultImageName() + "." + fileData.getResultExtension());
                                        try {
                                            ImageIO.write(computedImageFromBounds, fileData.getResultExtension(), resultFile);
                                        } catch (IOException e) {
                                            LOGGER.error("Error while writting computed image {}", fileData.getResultImageName(), e);
                                        }

                                    } catch (IOException e) {
                                        try {
                                            Files.copy(fileData.getFile(), new File(targetFolderKO + "/" + fileData.getResultImageName() + "." + fileData.getResultExtension()));
                                        } catch (IOException ee) {
                                            LOGGER.error("Error while moving {} in error to {}", fileData.getFile().getName(), targetFolderKO);
                                        }
                                        LOGGER.error("Error while reading {}", fileData.getFile().getName(), e);
                                    }
                                });

//                        Stopwatch xyTimer = Stopwatch.createStarted();
//                        final Map<String, Integer> xyBoundsFromImageList = ImageUtils.getXYBoundsFromImageList(bufferedImageList);
//                        LOGGER.info("xy bounds found for {} in {}", stringListEntry.getKey(), xyTimer.stop());
//
//                        fileDatas.stream()
//                                .forEach(fileData -> {
//                                    final Stopwatch imageTimer = Stopwatch.createStarted();
//                                    final BufferedImage computedImageFromBounds = ImageUtils.getComputedImageFromBounds(fileData.getBufferedImage(),
//                                            Template.F9.getWidth(), Template.F9.getHeight(), Template.F9.getMargin(), 1100, 1200, xyBoundsFromImageList);
//                                    LOGGER.info("{} processed in {}", fileData.getResultImageName(), imageTimer.stop());
//                                    final File resultFile = new File(targetFolder + "/" + fileData.getResultImageName() + "." + fileData.getResultExtension());
//                                    try {
//                                        ImageIO.write(computedImageFromBounds, fileData.getResultExtension(), resultFile);
//                                    } catch (IOException e) {
//                                        LOGGER.error("Error while writting computed image {}", fileData.getResultImageName(), e);
//                                    }
//                                });

                        LOGGER.info("{} images processed for ZP {} in {}", fileDatas.size(), stringListEntry.getKey(), productTimer.stop());

                    });


            LOGGER.info("{} supplier image(s) & {} gun image(s) processed in {}", supplierImage, gunImage, timer.stop());
        }

        System.exit(0);


    }
}
