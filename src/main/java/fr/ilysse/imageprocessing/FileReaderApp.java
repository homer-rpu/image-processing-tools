package fr.ilysse.imageprocessing;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by p_poucif on 03/04/2017.
 */
public class FileReaderApp {

    public static void main(String[] args) {


        Path firstRecup = Paths.get("C:\\Users\\p_poucif\\Desktop\\rattrapage\\ug_products_rattrapage_livraison.txt");
        Path secondRecup = Paths.get("C:\\Users\\p_poucif\\Desktop\\rattrapage\\rattrapage_rattrapage.txt");

        BufferedReader brO;
        BufferedReader brI;

        try {
            brO = new BufferedReader(new FileReader(firstRecup.toFile()));
            brI = new BufferedReader(new FileReader(secondRecup.toFile()));


            String line = brI.readLine();
            final String[] split = line.split(",");

            String lineToSearch = brO.readLine();
            StringBuilder sb = new StringBuilder();

            int n = 0;

            for (int i = 0; i < split.length; i++) {
                String idMoco = split[i];
                String[] secondSplit = idMoco.split("&");
                if (lineToSearch.contains(secondSplit[0])) {
                    sb.append(secondSplit[0]).append(";");
                    n++;
                }
            }

            System.out.println(n);


        } catch (IOException e) {

        }


    }

}
