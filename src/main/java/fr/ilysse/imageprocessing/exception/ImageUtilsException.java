package fr.ilysse.imageprocessing.exception;

/**
 * Created by pierre on 13/05/17.
 */
public class ImageUtilsException extends Exception {
    public ImageUtilsException(String s) {
        super(s);
    }

    public ImageUtilsException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public ImageUtilsException(Throwable throwable) {
        super(throwable);
    }
}
