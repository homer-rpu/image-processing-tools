package fr.ilysse.imageprocessing;

import com.google.common.base.Stopwatch;
import com.google.common.io.Files;
import fr.ilysse.imageprocessing.data.FileData;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.Properties;

/**
 * Created by p_poucif on 24/04/2017.
 */
public class RenameApp {
    public static final Logger LOGGER = LoggerFactory.getLogger(RenameApp.class);
    public static final Collection<String> EXTENSIONS = Arrays.asList("jpg", "png", "jpeg");
    public static final String GUN_SEPARATOR = "_";
    public static final String GUN_PREFIX = "G_";
    public static final String ZP = "_ZP_";

    public static void main(String[] args) {

        Stopwatch timer = Stopwatch.createStarted();
        if (args.length < 1) {
            LOGGER.error("Missing application properties file");
            System.exit(-1);
        }

        Properties properties = null;
        try {
            properties = new Properties();
            properties.load(java.nio.file.Files.newInputStream(Paths.get(args[0])));
        } catch (IOException e) {
            LOGGER.error("Unable to read application properties file", e);
            System.exit(-1);
        }

        String sourceFolderString = properties.getProperty("sourceFolder");
        String targetFolder = properties.getProperty("targetFolder");

        Path sourcePath = Paths.get(sourceFolderString);
        File sourceFolder = sourcePath.toFile();
        if (sourceFolder.exists()) {

            final File[] files = sourceFolder.listFiles();

            for (final File file : files) {
                if (EXTENSIONS.contains(Files.getFileExtension(file.getName()))) {

                    String ugProduct = null;
                    FileData fileData = null;

                    if (StringUtils.startsWith(file.getName(), GUN_PREFIX)) {
                        final String[] split = Files.getNameWithoutExtension(file.getName()).split(GUN_SEPARATOR);
                        if (split.length == 4) {
                            final String resultImageName = GUN_PREFIX + split[1] + GUN_SEPARATOR + split[2] + ZP + split[3] + "." + Files.getFileExtension(file.getName());
                            try {
                                Files.copy(file, new File(targetFolder + "/" + resultImageName));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }


                        } else {
                            LOGGER.warn("Incorrect gun file name {}", file.getName());
                        }
                    }

                }
            }

        }

        System.exit(0);


    }
}
