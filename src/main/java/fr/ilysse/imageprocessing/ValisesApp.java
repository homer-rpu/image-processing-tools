package fr.ilysse.imageprocessing;

import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.io.Files;
import com.sun.org.apache.bcel.internal.generic.IUSHR;
import fr.ilysse.imageprocessing.data.FileData;
import fr.ilysse.imageprocessing.data.Template;
import fr.ilysse.imageprocessing.image.ImageUtils;
import fr.ilysse.imageprocessing.image.ImageWriter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.nio.ch.IOUtil;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by pierre on 24/05/17.
 */
public class ValisesApp {

    public static final Logger LOGGER = LoggerFactory.getLogger(ValisesApp.class);
    public static final Collection<String> EXTENSIONS = Arrays.asList("jpg", "png", "jpeg");
    public static final String GUN_SEPARATOR = "_";
    public static final String GUN_PREFIX = "G_";
    public static final String ZP = "_ZP_";

    public static void main(String[] args){
        if (args.length < 1) {
            LOGGER.error("Missing application properties file");
            System.exit(-1);
        }

        Properties properties = null;
        try {
            properties = new Properties();
            properties.load(java.nio.file.Files.newInputStream(Paths.get(args[0])));
        } catch (IOException e) {
            LOGGER.error("Unable to read application properties file", e);
            System.exit(-1);
        }

        String sourceFolderString = properties.getProperty("sourceFolder");
        String targetFolderOK = properties.getProperty("targetFolderOK");
        String targetFolderKO = properties.getProperty("targetFolderKO");

        Path sourcePath = Paths.get(sourceFolderString);
        File sourceFolder = sourcePath.toFile();
        if (sourceFolder.exists()) {

            int imageProcessedCount = 0;
            int gunImage = 0;
            int supplierImage = 0;
            int errorImage = 0;

            final File[] files = sourceFolder.listFiles();
            Map<String, List<FileData>> imagesMappedToUgProduct = Maps.newHashMap();
            Stopwatch fileTimer = Stopwatch.createStarted();


            for (final File file : files) {
                if (EXTENSIONS.contains(Files.getFileExtension(file.getName()))) {

                    String ugProduct = null;
                    FileData fileData = null;

                    if (StringUtils.startsWith(file.getName(), GUN_PREFIX)) {
                        final String[] split = Files.getNameWithoutExtension(file.getName()).split(GUN_SEPARATOR);
                        if (split.length == 5) {
                                ugProduct = split[1] + GUN_SEPARATOR + split[4];
                                final String resultImageName = GUN_PREFIX + split[1] + GUN_SEPARATOR + split[2] + ZP + split[4];
                                fileData = new FileData(file, targetFolderOK, resultImageName, Files.getFileExtension(file.getName()));
                                gunImage++;
                        } else {
                            LOGGER.warn("Incorrect gun file name {}", file.getName());
                            errorImage++;
                        }
                    }

                    if (StringUtils.isNoneBlank(ugProduct) && fileData != null) {
                        if (imagesMappedToUgProduct.containsKey(ugProduct)) {
                            imagesMappedToUgProduct.get(ugProduct).add(fileData);
                        } else {
                            imagesMappedToUgProduct.put(ugProduct, Lists.newArrayList(fileData));
                        }
                    }

                    imageProcessedCount++;
                }
            }

            LOGGER.info("{} image(s) found over {} file(s) in {} in {}", imageProcessedCount, files.length, sourceFolderString, fileTimer.stop());
            LOGGER.warn("{} error image(s) found", errorImage);
            LOGGER.info("*****************************************************");
            LOGGER.info("************* Starting Image Processing *************");
            LOGGER.info("*****************************************************");


            imagesMappedToUgProduct.entrySet().stream()
                    .forEach(stringListEntry -> {
                        LOGGER.info("//////////////// Starting to process {} image(s) for {} ////////////////", stringListEntry.getValue().size(), stringListEntry.getKey());
                        final Stopwatch productTimer = Stopwatch.createStarted();

                        final List<FileData> fileDatas = stringListEntry.getValue();

                        fileDatas.stream()
                                .forEach(fileData -> {
                                    try {
                                        final Stopwatch imageTimer = Stopwatch.createStarted();
                                        final BufferedImage bufferedImage = ImageIO.read(fileData.getFile());
                                        fileData.setBufferedImage(bufferedImage);

                                        if (fileData.getResultImageName().split(GUN_SEPARATOR)[4].equals("1")) {
                                            if(ImageUtils.isSquareImage(bufferedImage)){
                                                ImageWriter.writeImageToNamedFolder(fileData,"OK",bufferedImage);
                                            }else{
                                                ImageWriter.writeImageToNamedFolder(fileData,"a_retoucher",bufferedImage);
                                            }
                                        }


                                        LOGGER.info("{} processed in {}", fileData.getResultImageName(), imageTimer.stop());

                                    } catch (IOException e) {
                                        try {
                                            ImageWriter.copyKOImage(fileData);
                                        } catch (IOException ee) {
                                            LOGGER.error("Error while moving {} in error to {}", fileData.getFile().getName(), targetFolderKO);
                                        }
                                        LOGGER.error("Error while reading {}", fileData.getFile().getName(), e);
                                    }
                                });

                        LOGGER.info("{} images processed for ZP {} in {}", fileDatas.size(), stringListEntry.getKey(), productTimer.stop());

                    });


        }






    }
}
