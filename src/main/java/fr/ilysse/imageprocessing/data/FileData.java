package fr.ilysse.imageprocessing.data;

import java.awt.image.BufferedImage;
import java.io.File;

/**
 * Created by p_poucif on 11/04/2017.
 */
public class FileData {

    private File file;
    private String resultPath;
    private String resultImageName;
    private String resultExtension;
    private BufferedImage bufferedImage;

    public FileData(File file, String resultImageName, String resultExtension) {
        this.file = file;
        this.resultImageName = resultImageName;
        this.resultExtension = resultExtension;
    }

    public FileData(File file, String resultPath, String resultImageName, String resultExtension) {
        this.file = file;
        this.resultPath = resultPath;
        this.resultImageName = resultImageName;
        this.resultExtension = resultExtension;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getResultPath() {
        return resultPath;
    }

    public void setResultPath(String resultPath) {
        this.resultPath = resultPath;
    }

    public void setResultImageName(String resultImageName) {
        this.resultImageName = resultImageName;
    }

    public void setResultExtension(String resultExtension) {
        this.resultExtension = resultExtension;
    }

    public File getFile() {
        return file;
    }

    public String getResultImageName() {
        return resultImageName;
    }

    public String getResultExtension() {
        return resultExtension;
    }

    public BufferedImage getBufferedImage() {
        return bufferedImage;
    }

    public void setBufferedImage(BufferedImage bufferedImage) {
        this.bufferedImage = bufferedImage;
    }
}
