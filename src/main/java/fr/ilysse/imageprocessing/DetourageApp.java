package fr.ilysse.imageprocessing;

import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.io.Files;
import fr.ilysse.imageprocessing.clustering.ClusterUtils;
import fr.ilysse.imageprocessing.color.converter.ColorConverterData;
import fr.ilysse.imageprocessing.data.FileData;
import fr.ilysse.imageprocessing.exception.ImageUtilsException;
import fr.ilysse.imageprocessing.image.CuttingUtils;
import fr.ilysse.imageprocessing.image.HistogramUtils;
import fr.ilysse.imageprocessing.image.ImageUtils;
import fr.ilysse.imageprocessing.image.ImageWriter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.List;

/**
 * Created by p_poucif on 18/04/2017.
 */
public class DetourageApp {

    public static final Logger LOGGER = LoggerFactory.getLogger(DetourageApp.class);
    public static final Collection<String> EXTENSIONS = Arrays.asList("jpg", "png", "jpeg");
    public static final String GUN_SEPARATOR = "_";
    public static final String GUN_PREFIX = "G_";
    public static final String ZP = "_ZP_";

    public static void main(String[] args) {
        Stopwatch timer = Stopwatch.createStarted();
        if (args.length < 1) {
            LOGGER.error("Missing application properties file");
            System.exit(-1);
        }

        Properties properties = null;
        try {
            properties = new Properties();
            properties.load(java.nio.file.Files.newInputStream(Paths.get(args[0])));
        } catch (IOException e) {
            LOGGER.error("Unable to read application properties file", e);
            System.exit(-1);
        }

        String sourceFolderString = properties.getProperty("sourceFolder");
        String targetFolderOK = properties.getProperty("targetFolderOK");
        String targetFolderKO = properties.getProperty("targetFolderKO");

        Path sourcePath = Paths.get(sourceFolderString);
        File sourceFolder = sourcePath.toFile();
        if (sourceFolder.exists()) {

            int imageProcessedCount = 0;
            int gunImage = 0;
            int supplierImage = 0;
            int errorImage = 0;

            final File[] files = sourceFolder.listFiles();
            Map<String, List<FileData>> imagesMappedToUgProduct = Maps.newHashMap();
            Stopwatch fileTimer = Stopwatch.createStarted();


            for (final File file : files) {
                if (EXTENSIONS.contains(Files.getFileExtension(file.getName()))) {

                    String ugProduct = null;
                    FileData fileData = null;

                    if (StringUtils.startsWith(file.getName(), GUN_PREFIX)) {
                        final String[] split = Files.getNameWithoutExtension(file.getName()).split(GUN_SEPARATOR);
                        if (split.length == 5) {
                            ugProduct = split[1] + GUN_SEPARATOR + split[4];
                            final String resultImageName = GUN_PREFIX + split[1] + GUN_SEPARATOR + split[2] + ZP + split[4];
                            fileData = new FileData(file, targetFolderOK, resultImageName, Files.getFileExtension(file.getName()));
                            gunImage++;
                        } else {
                            LOGGER.warn("Incorrect gun file name {}", file.getName());
                            errorImage++;
                        }
                    }

                    if (StringUtils.isNoneBlank(ugProduct) && fileData != null) {
                        if (imagesMappedToUgProduct.containsKey(ugProduct)) {
                            imagesMappedToUgProduct.get(ugProduct).add(fileData);
                        } else {
                            imagesMappedToUgProduct.put(ugProduct, Lists.newArrayList(fileData));
                        }
                    }

                    imageProcessedCount++;
                }
            }

            LOGGER.info("{} image(s) found over {} file(s) in {} in {}", imageProcessedCount, files.length, sourceFolderString, fileTimer.stop());
            LOGGER.warn("{} error image(s) found", errorImage);
            LOGGER.info("*****************************************************");
            LOGGER.info("************* Starting Image Processing *************");
            LOGGER.info("*****************************************************");

            imagesMappedToUgProduct.entrySet().stream()
                    .forEach(stringListEntry -> {
                        LOGGER.info("//////////////// Starting to process {} image(s) for {} ////////////////", stringListEntry.getValue().size(), stringListEntry.getKey());
                        final Stopwatch productTimer = Stopwatch.createStarted();

                        final List<FileData> fileDatas = stringListEntry.getValue();

                        fileDatas.stream()
                                .forEach(fileData -> {
                                    try {
                                        final Stopwatch imageTimer = Stopwatch.createStarted();
                                        final BufferedImage bufferedImage = ImageIO.read(fileData.getFile());
                                        fileData.setBufferedImage(bufferedImage);

                                        ImageWriter.writeImage(fileData, "0", "initial", bufferedImage);
                                        final BufferedImage convolveImageWithMask = ImageUtils.convolveImageWithMask(bufferedImage, ImageUtils.getBlurMask2());
                                        ;
                                        try {
                                            final BufferedImage differentialImage = ImageUtils.getDifferentialImage(bufferedImage, convolveImageWithMask);
                                            ImageWriter.writeImage(fileData, "1", "diff", differentialImage);

                                            final int[] greyImageHistogram = HistogramUtils.getGreyImageHistogram(differentialImage);
                                            final BufferedImage imageHistogram = HistogramUtils.getImageHistogram(greyImageHistogram);
                                            ImageWriter.writeImage(fileData, "2", "histogram", imageHistogram);

                                            final BufferedImage histogram = HistogramUtils.getImageHistogram(HistogramUtils.rescaleHistogram(greyImageHistogram).get("histogram"));
                                            ImageWriter.writeImage(fileData, "3", "rescaled", histogram);
//                                            final BufferedImage equalizedGreyScaleImage = HistogramUtils.getEqualizedGreyScaleImage(convolveImageWithMask);
//                                            ImageWriter.writeImage(fileData, "4", "equalized", equalizedGreyScaleImage);
                                        } catch (ImageUtilsException e) {
                                            LOGGER.error(e.getMessage());
                                        }

//                                        final BufferedImage convolveImageWithMask
//                                                = ImageUtils.convolveImageWithMask(bufferedImage, ImageUtils.getEdgeDetectionMask());
//                                        ImageWriter.writeImage(fileData, "1", "gradient", convolveImageWithMask);


                                        //                                        final BufferedImage sharpenImage = ImageUtils.getCroppedImageWithConvovle(bufferedImage, ImageUtils.getSharpenMask2());
//                                        ImageWriter.writeImage(fileData,"1","sharpen",sharpenImage);

//                                        final BufferedImage computedImageFromBounds = ImageUtils.getComputedImageWithBackgroundColor(fileData,
//                                                Template.F9.getWidth(), Template.F9.getHeight(), Template.F9.getMargin(), 1100, 1200);

                                        LOGGER.info("{} processed in {}", fileData.getResultImageName(), imageTimer.stop());
                                        final File resultFile = new File(targetFolderOK + "/" + fileData.getResultImageName() + "." + fileData.getResultExtension());
//                                        try {
//                                            ImageIO.write(computedImageFromBounds, fileData.getResultExtension(), resultFile);
//                                        } catch (IOException e) {
//                                            LOGGER.error("Error while writting computed image {}", fileData.getResultImageName(), e);
//                                        }

                                    } catch (IOException e) {
                                        try {
                                            ImageWriter.copyKOImage(fileData);
                                        } catch (IOException ee) {
                                            LOGGER.error("Error while moving {} in error to {}", fileData.getFile().getName(), targetFolderKO);
                                        }
                                        LOGGER.error("Error while reading {}", fileData.getFile().getName(), e);
                                    }
                                });

                        LOGGER.info("{} images processed for ZP {} in {}", fileDatas.size(), stringListEntry.getKey(), productTimer.stop());

                    });


        }
        System.exit(0);
    }
}
