package fr.ilysse.imageprocessing.image;

import fr.ilysse.imageprocessing.data.FileData;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.io.File;

/**
 * Created by p_poucif on 05/05/2017.
 */
public class ImageWriter {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImageWriter.class);
    private static final String KO = "KO";

    public static void writeImage(final FileData fileData, final String stepNumber, final String stepName, final BufferedImage bufferedImage) {
        final String path = fileData.getResultPath();
        final String imageName = fileData.getResultImageName();
        final String extension = fileData.getResultExtension();
        String folderName = getFolderName(path, imageName);

        try {
            Path folderPath = Paths.get(folderName);
            if (Files.notExists(folderPath)) {
                Files.createDirectories(folderPath);
            }

            String resultingImage = stepNumber + "_" + com.google.common.io.Files.getNameWithoutExtension(imageName) + "_" + stepName + "." + extension;
            Path imagePath = Paths.get(folderName + "/" + resultingImage);
            ImageIO.write(bufferedImage, extension, imagePath.toFile());

        } catch (IOException e) {
            LOGGER.error("Error while writing image {} for step {}", imageName, stepName, e);
        }

    }

    public static void writeImageToNamedFolder(final FileData fileData, final String folderName, final BufferedImage bufferedImage) {
        final String path = fileData.getResultPath();
        final String resultImageName = fileData.getResultImageName();
        final String resultFolder = getFolderName(path, folderName);

        try {
            Path folderPath = Paths.get(resultFolder);
            if (Files.notExists(folderPath)) {
                Files.createDirectories(folderPath);
            }

            final Path imagePath = Paths.get(resultFolder + "/" + resultImageName + "." + fileData.getResultExtension());
            ImageIO.write(bufferedImage, fileData.getResultExtension(), imagePath.toFile());

        } catch (IOException e) {
            LOGGER.error("Error while writing image {} to folder {}", resultImageName, resultFolder);
        }
    }



    public static void copyKOImage(final FileData fileData) throws IOException {
        String folderName = getFolderName(fileData.getResultPath(), KO);
        Path koPath = Paths.get(folderName);

        if (Files.notExists(koPath)) {
            Files.createDirectories(koPath);
        }

        File file = new File(folderName + "/" + fileData.getResultImageName() + "." + fileData.getResultExtension());
        com.google.common.io.Files.copy(fileData.getFile(), file);
    }

    private static String getFolderName(final String path, final String folderName) {
        String finalFolderName;
        if (StringUtils.endsWith(path, "/")) {
            finalFolderName = path + com.google.common.io.Files.getNameWithoutExtension(folderName);
        } else {
            finalFolderName = path + "/" + com.google.common.io.Files.getNameWithoutExtension(folderName);
        }
        return finalFolderName;
    }

}
