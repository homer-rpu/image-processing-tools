package fr.ilysse.imageprocessing.image;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by pierre on 26/05/17.
 */
public class HistogramUtils {

    private final static Logger LOGGER = LoggerFactory.getLogger(HistogramUtils.class);
    private final static int HISTOGRAM_WIDTH = 770;
    private final static int HISTOGRAM_HEIGHT = 1000;
    private final static int HISTOGRAM_BAR_WIDTH = 1;
    private final static int HISTOGRAM_BAR_HEIGHT = 900;

    public static int[] getGreyImageHistogram(final BufferedImage bufferedImage) {
        int[] histogram = new int[256];
        for (int x = 0; x < bufferedImage.getWidth(); x++) {
            for (int y = 0; y < bufferedImage.getHeight(); y++) {
                int greyLevel = getGreyLevel(bufferedImage.getRGB(x, y));
                greyLevel = greyLevel > 255 ? 255 : greyLevel;
                greyLevel = greyLevel < 0 ? 0 : greyLevel;
                histogram[greyLevel]++;
            }
        }

        return histogram;
    }

    public static int getGreyLevel(int rgb) {
        final Color color = new Color(rgb);
        return Math.round((float) (color.getRed() + color.getGreen() + color.getBlue()) / 3);
    }

    public static BufferedImage getImageHistogram(final int[] histogram) {
        int yMax = 0;
        int xMax = 0;
        for (int i = 0; i < 256; i++) {
            if (yMax <= histogram[i]) {
                yMax = histogram[i];
                xMax = i;
            }
        }

        LOGGER.info("Level max = {} for grey scale = {}", yMax, xMax);

        final BufferedImage histogramImage = new BufferedImage(HISTOGRAM_WIDTH, HISTOGRAM_HEIGHT, BufferedImage.TYPE_INT_RGB);

        int xx = 0;
        for (int h = 0; h <= 255; h++) {
            xx = xx + 2;
            int yy = (HISTOGRAM_BAR_HEIGHT * histogram[h]) / yMax;
            LOGGER.info("h = {} and xx = {} and yy = {}", h, xx, yy);
            for (int x = xx; x <= xx + HISTOGRAM_BAR_WIDTH; x++) {
                for (int y = 0; y < yy; y++) {
                    try {
                        histogramImage.setRGB(x, HISTOGRAM_BAR_HEIGHT - y, Color.WHITE.getRGB());
                    } catch (Exception e) {
                        LOGGER.error("Exception for x={} and y={}", x, y, e);
                        return histogramImage;
                    }
                }
            }
            xx += HISTOGRAM_BAR_WIDTH;
        }
        return histogramImage;
    }

    public static Map<String, int[]> rescaleHistogram(final int[] histogram) {
        int[] rescaledHistogram = new int[256];
        int[] map = new int[256];

        int yMax = 0;
        int sum = 0;
        for (int h = 0; h < 256; h++) {
            sum += histogram[h];
            if (yMax < histogram[h]) {
                yMax = histogram[h];
            }
        }

        int s = 0;
        int hMax = 0;
        for (int h = 0; h < 256; h++) {
            if (s <= Math.round((float) 0.6 * sum)) {
                s += histogram[h];
                hMax = h;
            } else {
                break;
            }
        }

        int threshold = 200;
        for (int h = threshold; h < 256; h++) {
            int hh = Math.round(-(255F / 155) * h + (100 * 255) / 155);
            rescaledHistogram[hh] = histogram[h];
            map[hh] = h;
        }

        final Map<String, int[]> resultingMap = new HashMap<>();
        resultingMap.put("histogram", rescaledHistogram);
        resultingMap.put("map", map);
        return resultingMap;
    }


    public static BufferedImage getEqualizedGreyScaleImage(final BufferedImage bufferedImage) {
        final int[] greyImageHistogram = getGreyImageHistogram(bufferedImage);
        final Map<String, int[]> map = rescaleHistogram(greyImageHistogram);
        final int[] maps = map.get("map");

        final BufferedImage image = new BufferedImage(bufferedImage.getWidth(), bufferedImage.getHeight(), BufferedImage.TYPE_INT_RGB);
        for (int x = 0; x < image.getWidth(); x++) {
            for (int y = 0; y < image.getHeight(); y++) {
                int greyLevel = getGreyLevel(bufferedImage.getRGB(x, y));
                greyLevel = maps[greyLevel];
                image.setRGB(x, y, new Color(greyLevel, greyLevel, greyLevel).getRGB());
            }
        }
        return image;
    }


}
