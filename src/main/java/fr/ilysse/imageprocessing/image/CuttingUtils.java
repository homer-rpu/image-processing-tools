package fr.ilysse.imageprocessing.image;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by pierre on 23/05/17.
 */
public class CuttingUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(CuttingUtils.class);

    public static BufferedImage getDiscreteImage(final BufferedImage bufferedImage, final int squareXSize, final int squareYSize) {
        final BufferedImage image = new BufferedImage(bufferedImage.getWidth(), bufferedImage.getHeight(), BufferedImage.TYPE_INT_RGB);
        final int dx = Math.round(bufferedImage.getWidth() / squareXSize);
        final int dy = Math.round(bufferedImage.getHeight() / squareYSize);
        LOGGER.info("Starting to cut image to {} squares => {}dx * {}dy ", dx * dy, dx, dy);
        for (int x = 0; x < image.getWidth(); x++) {
            for (int y = 0; y < image.getHeight(); y++) {

                if (x % squareXSize == 0 || y % squareYSize == 0) {
                    image.setRGB(x, y, Color.RED.getRGB());
                } else {
                    image.setRGB(x, y, bufferedImage.getRGB(x, y));
                }

            }
        }

        return image;

    }

}
