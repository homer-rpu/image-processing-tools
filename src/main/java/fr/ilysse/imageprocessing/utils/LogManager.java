package fr.ilysse.imageprocessing.utils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.*;

/**
 * Created by p_poucif on 12/04/2017.
 */

public class LogManager {

    private static Logger logger = Logger.getLogger("ImpexBuilder");

    private static final String EOL = System.getProperty("line.separator");

    private LogManager() {
        // Prevent instantiation
    }

    public static void init(String logFile) throws IOException {
        Path pathToFile = Paths.get(logFile);
        if (Files.notExists(pathToFile)) {
            Files.createDirectories(pathToFile.getParent());
            Files.createFile(pathToFile);
        } else {
            Files.delete(pathToFile);
        }

        Handler fileHandler = new FileHandler(logFile);
        fileHandler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
                return dateFormat.format(new Date()) + " : " + record.getMessage() + EOL;
            }
        });
        fileHandler.setEncoding(StandardCharsets.UTF_8.name());

        logger.addHandler(fileHandler);
        logger.setUseParentHandlers(false);
    }

    public static Logger getLogger() {
        return logger;
    }

}
